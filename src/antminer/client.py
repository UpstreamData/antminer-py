import requests
from requests.auth import HTTPDigestAuth


class AntMiner:
    def __init__(self, host: str, username: str, password: str):
        self.host = host
        self.username = username
        self.password = password

    @property
    def commands(self):
        return [
            func
            for func in
            # each function in self
            dir(self)
            if not func in ["commands", "get"]
            # no uri properties
            if not func.endswith("_uri") and
            # no hidden attributes
            not func.startswith("__")
        ]


    @property
    def base_uri(self):
        return f"http://{self.username}:{self.password}@{self.host}/cgi-bin"

    def get(self, uri: str):
        return requests.get(
            uri,
            auth=HTTPDigestAuth(self.username, self.password),
            verify=False,
            stream=True,
        )

    @property
    def blink_uri(self):
        return f"{self.base_uri}/blink.cgi"

    @property
    def chart_uri(self):
        return f"{self.base_uri}/chart.cgi"

    @property
    def create_log_backup_uri(self):
        return f"{self.base_uri}/create_log_backup.cgi"

    @property
    def dlog_uri(self):
        return f"{self.base_uri}/dlog.cgi"

    @property
    def get_blink_status_uri(self):
        return f"{self.base_uri}/get_blink_status.cgi"

    @property
    def get_droa_uri(self):
        return f"{self.base_uri}/get_droa.cgi"

    @property
    def get_miner_conf_uri(self):
        return f"{self.base_uri}/get_miner_conf.cgi"

    @property
    def get_network_info_uri(self):
        return f"{self.base_uri}/get_network_info.cgi"

    @property
    def get_system_info_uri(self):
        return f"{self.base_uri}/get_system_info.cgi"

    @property
    def hlog_uri(self):
        return f"{self.base_uri}/hlog.cgi"

    @property
    def log_uri(self):
        return f"{self.base_uri}/log.cgi"

    @property
    def miner_type_uri(self):
        return f"{self.base_uri}/miner_type.cgi"

    @property
    def passwd_uri(self):
        return f"{self.base_uri}/passwd.cgi"

    @property
    def pools_uri(self):
        return f"{self.base_uri}/pools.cgi"

    @property
    def reboot_uri(self):
        return f"{self.base_uri}/reboot.cgi"

    @property
    def reset_conf_uri(self):
        return f"{self.base_uri}/reset_conf.cgi"

    @property
    def set_miner_conf_uri(self):
        return f"{self.base_uri}/set_miner_conf.cgi"

    @property
    def set_network_conf_uri(self):
        return f"{self.base_uri}/set_network_conf.cgi"

    @property
    def stats_uri(self):
        return f"{self.base_uri}/stats.cgi"

    @property
    def summary_uri(self):
        return f"{self.base_uri}/summary.cgi"

    @property
    def upgrade_uri(self):
        return f"{self.base_uri}/upgrade.cgi"

    @property
    def upgrade_clear_uri(self):
        return f"{self.base_uri}/upgrade_clear.cgi"

    @property
    def blink(self):
        return self.get(self.blink_uri).text

    @property
    def chart(self):
        return self.get(self.chart_uri).text

    @property
    def create_log_backup(self):
        return self.get(self.create_log_backup_uri).text

    @property
    def dlog(self):
        return self.get(self.dlog_uri).text

    @property
    def get_blink_status(self):
        return self.get(self.get_blink_status_uri).text

    @property
    def get_droa(self):
        return self.get(self.get_droa_uri).text

    @property
    def get_miner_conf(self):
        return self.get(self.get_miner_conf_uri).text

    @property
    def get_network_info(self):
        return self.get(self.get_network_info_uri).text

    @property
    def get_system_info(self):
        return self.get(self.get_system_info_uri).text

    @property
    def hlog(self):
        return self.get(self.hlog_uri).text

    @property
    def log(self):
        return self.get(self.log_uri).text

    @property
    def miner_type(self):
        return self.get(self.miner_type_uri).text

    @property
    def passwd(self):
        return self.get(self.passwd_uri).text

    @property
    def pools(self):
        return self.get(self.pools_uri).text

    @property
    def reboot(self):
        return self.get(self.reboot_uri).text

    @property
    def reset_conf(self):
        return self.get(self.reset_conf_uri).text

    @property
    def set_miner_conf(self):
        return self.get(self.set_miner_conf_uri).text

    @property
    def set_network_conf(self):
        return self.get(self.set_network_conf_uri).text

    @property
    def stats(self):
        return self.get(self.stats_uri).text

    @property
    def summary(self):
        return self.get(self.summary_uri).text

    @property
    def upgrade(self):
        return self.get(self.upgrade_uri).text

    @property
    def upgrade_clear(self):
        return self.get(self.upgrade_clear_uri).text
