POETRY_BIN ?= ~/.local/bin/poetry

# ------------------------------------------
# dependency management
# ------------------------------------------

lock-no-update:
	$(POETRY_BIN) lock --no-update

lock-update:
	$(POETRY_BIN) lock

poetry-check-lock:
	$(POETRY_BIN) lock --check

# ------------------------------------------
# git
# ------------------------------------------
setup-precommit:
	rm -rf .git/hooks
	git config --unset-all core.hooksPath
	$(POETRY_BIN) run pre-commit install

hooks: setup-precommit

# ------------------------------------------
# newsfragments
# ------------------------------------------

towncrier_draft:
	$(POETRY_BIN) run towncrier --draft

towncrier_build:
	$(POETRY_BIN) run towncrier
