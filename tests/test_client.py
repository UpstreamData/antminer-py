import json

import pytest

from antminer.client import AntMiner

from .data import STATS


def asjson(function):
    def wrapper(data, endpoint):
        return json.dumps(function(data, endpoint))

    return wrapper


class TestBosminer:
    @asjson
    def data(self, endpoint):
        d = {
            "stats": STATS,
        }
        return d[endpoint]

    @pytest.fixture
    def client(self):
        return AntMiner("test", "root", "root")

    @pytest.mark.parametrize(
        "uri", ["log", "hlog", "stats", "summary", "pools", "chart", "reboot"]
    )
    def test_uri(self, client, uri):
        assert getattr(client, f"{uri}_uri") == f"{client.base_uri}/{uri}.cgi"

    @pytest.mark.parametrize(
        "endpoint",
        [
            "stats",
        ],
    )
    def test_json_endpoint(
        self,
        mocker,
        client,
        endpoint,
    ):
        data = self.data(endpoint)
        mocker.patch("requests.get").return_value.json.return_value = data
        assert getattr(client, endpoint) == data
